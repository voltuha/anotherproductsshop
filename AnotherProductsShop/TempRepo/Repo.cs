﻿using System;
using System.Collections.Generic;
using AnotherProductsShop.Models;

namespace AnotherProductsShop.TempRepo
{
    public static class Repo
    {
        public static List<User> _Users = new List<User>();
        public static List<Product> _Products = new List<Product>();
        static Repo()
        {
            for (int i = 0; i < 10; i++)
            {
                _Users.Add(new User
                {
                    Age = new Random().Next(15, 70),
                    Id = Guid.NewGuid(),
                    Email = $"User{i}Email@example.com",
                    Name = $"UserName{i}",
                    RegistrationDate = DateTime.Now,
                    Status = UserStatus.Active,
                });

            }

            for (int i = 0; i < _Users.Count; i++)
            {
                _Users[i].UserRelatedProducts = new List<Product>();
                _Users[i].UserRelatedProducts.Add(
                    new Product("Product Title" + i, 100 + i * 50, _Users[i].Id));
                for (int k = 0; k < _Users[i].UserRelatedProducts.Count; k++)
                {
                    _Products.Add(_Users[i].UserRelatedProducts[k]);
                }
            }
        }



    }
}


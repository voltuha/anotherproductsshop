﻿ using System;
namespace AnotherProductsShop.Models
{
    public class Product
    {
        public Guid Id { get; private set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        private DateTime PostTime { get; set; }
        public Guid SellerId { get; private set; }
        public Guid? BuyerId { get; set; }
        public ProductStatus ProductStatus { get; set; }

        public Product()
        {

        }

        public Product(string title, decimal price, Guid productOwner)
        {
            Id = Guid.NewGuid();
            Title = title;
            Price = price;
            PostTime = DateTime.Now;
            SellerId = productOwner;
            BuyerId = null;
            ProductStatus = ProductStatus.Active;
        }
    }

    public enum ProductStatus
    {
        Active,
        Sold
    }

}

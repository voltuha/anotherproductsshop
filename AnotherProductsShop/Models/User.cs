﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AnotherProductsShop.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        //[Required]
        public string Email { get; set; }
        //[Required]
        //[RegularExpression(@"^[0-9\+]*$", ErrorMessage = "Can contains only digits and '+' symbol")]
        public string Password { get; set; }
        public int Age { get; set; }
        public DateTime RegistrationDate { get; set; }
        public List<Product> UserRelatedProducts { get; set; }
        public UserStatus Status { get; set; }

        public User(string email, string password)
        {
            Id = Guid.NewGuid();
            Name = "UserName";
            Email = email;
            Password = password;
            Age = 18;
            RegistrationDate = DateTime.Now;
            UserRelatedProducts = null;
            Status = UserStatus.Active;
        }

        public User()
        {

        }
    }

}

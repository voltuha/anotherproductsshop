﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace AnotherProductsShop.Extensions
{
    public static class CommonExtensions
    {
        public static Guid GetUserIdFromContext(this HttpContext context)
        {
            if(context.User == null)
            {
                return Guid.Empty;
            }

            return Guid.Parse(context.User.Claims.Single(x => x.Type == "id").Value);
        }
    }
}

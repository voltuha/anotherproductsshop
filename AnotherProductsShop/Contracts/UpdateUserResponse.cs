﻿using System;
using System.Collections.Generic;
using AnotherProductsShop.Models;

namespace AnotherProductsShop.Contracts
{
    public class UpdateUserResponse
    {
        public User User { get; set; }

        public IEnumerable<string> Errors { get; set; }
    }
}

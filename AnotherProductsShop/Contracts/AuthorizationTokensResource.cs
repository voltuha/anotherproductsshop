namespace AnotherProductsShop.Contracts
{
    public class AuthorizationTokensResource
    {
        public TokenResource AcessToken { get; set; }
    }
}
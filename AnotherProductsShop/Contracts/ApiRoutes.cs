﻿using System;
namespace AnotherProductsShop.Contracts
{
    public static class ApiRoutes
    {
        public static class Users
        {
            public const string GetAllUsers = "api/Users";

            public const string GetUser = "api/Users/{userId}";

            public const string UpdateUser = "api/Users/{userId}";

            public const string DeleteUser = "api/Users/{userId}";

            public const string Register = "api/Registration/";

            public const string Login = "api/Login";

        }

        public static class Products
        {
            public const string GetAllProducts = "api/Products";

            public const string GetProduct = "api/Products/{productId}";

            public const string UpdateProduct = "api/Products/{productId}";

            public const string DeleteProduct = "api/Products/{productId}";

            public const string CreateProduct = "api/Products/";

            public const string BuyProduct = "api/Buy/{productId}";
        }
    }
}

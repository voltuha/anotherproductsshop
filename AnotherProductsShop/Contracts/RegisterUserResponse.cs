﻿using System;
using System.Collections.Generic;
using AnotherProductsShop.Models;

namespace AnotherProductsShop.Contracts
{
    public class RegisterUserResponse
    {
        public DomainUser User { get; set; }
        public string Token { get; set; }
    }
}

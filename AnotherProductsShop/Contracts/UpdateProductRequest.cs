﻿using System;
namespace AnotherProductsShop.Contracts
{
    public class UpdateProductRequest
    {
        public string Title { get; set; }
        public decimal Price { get; set; }
    }
}

using System.ComponentModel.DataAnnotations;

namespace AnotherProductsShop.Contracts.Facebook
{
    public class FacebookLoginResource
    {
        [Required]
        [StringLength(255)]
        public string FacebookToken { get; set; }
    }
}
namespace AnotherProductsShop.Contracts.Facebook
{
    public class FacebookUserResource
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
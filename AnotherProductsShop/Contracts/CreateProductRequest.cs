﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AnotherProductsShop.Contracts
{
    public class CreateProductRequest
    {
        public string Title { get; set; }
        public decimal Price { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace AnotherProductsShop.Models
{
    //This model is used to return restricted user model. Kinda shit thing, I know.
    public class DomainUser
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        //[Required]
        public string Email { get; set; }
        //[Required]
        //[RegularExpression(@"^[0-9\+]*$", ErrorMessage = "Can contains only digits and '+' symbol")]
        public int Age { get; set; }
        public DateTime RegistrationDate { get; set; }
        public List<Product> UserRelatedProducts { get; set; }
        public UserStatus Status { get; set; }

    }

    public enum UserStatus
    {
        Active,
        Banned
    }
}

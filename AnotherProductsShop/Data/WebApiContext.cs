﻿using System;
using AnotherProductsShop.Models;
using Microsoft.EntityFrameworkCore;

namespace AnotherProductsShop.Data
{
    public class WebApiContext : DbContext
    {
        public WebApiContext(DbContextOptions<WebApiContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        
    }
}

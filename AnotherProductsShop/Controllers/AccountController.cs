using System.Threading.Tasks;
using AnotherProductsShop.Contracts.Facebook;
using AnotherProductsShop.Services;
using Microsoft.AspNetCore.Mvc;

namespace AnotherProductsShop.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        
        [HttpPost]
        [Route("account/login/facebook")]
        public async Task<IActionResult> FacebookLoginAsync([FromBody] FacebookLoginResource resource)
        {
            var authorizationTokens = await _accountService.FacebookLoginAsync(resource);
            return Ok(authorizationTokens);
        }
    }
}
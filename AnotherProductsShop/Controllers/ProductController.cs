﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AnotherProductsShop.Contracts;
using AnotherProductsShop.Extensions;
using AnotherProductsShop.Models;
using AnotherProductsShop.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AnotherProductsShop.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [AllowAnonymous]
        [HttpGet(ApiRoutes.Products.GetAllProducts)]
        public async Task<IActionResult> GetAllProducts() =>
            Ok(await _productService.GetAllProductsAsync());

        [AllowAnonymous]
        [HttpGet(ApiRoutes.Products.GetProduct)]
        public async Task<IActionResult> GetProduct(Guid productId)
        {
            var product = await _productService.GetProductAsync(productId);
            if (product != null)
                return Ok(product);
            return NotFound();
        }

        [HttpPost(ApiRoutes.Products.CreateProduct)]
        public async Task<IActionResult> CreateProduct([FromBody]CreateProductRequest request)
        {
            var product = new Product(request.Title, request.Price, HttpContext.GetUserIdFromContext());
            await _productService.CreateProductAsync(product);
            if (product != null)
            {
                var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}";
                var locationUri = baseUrl + "/" + ApiRoutes.Products.CreateProduct.Replace("{productId}", product.Id.ToString());
                return Created(locationUri, product);
            }
            return NotFound($"Seller id {HttpContext.GetUserIdFromContext()} is not exists");
                
        }

        [HttpDelete(ApiRoutes.Products.DeleteProduct)]
        public async Task<IActionResult> DeleteProduct([FromRoute] Guid productId)
        {
            var userOwnsProduct = await _productService.UsersOwnProduct(productId, HttpContext.GetUserIdFromContext());
            if (!userOwnsProduct)
                return BadRequest("You can delete only your own products");

            var isDeleted = await _productService.DeleteProductAsync(productId);

            return isDeleted ? Ok() : (IActionResult)NotFound();
        }

        [HttpPut(ApiRoutes.Products.UpdateProduct)]
        public async Task<IActionResult> UpdateProduct([FromRoute]Guid productId, [FromBody] UpdateProductRequest request)
        {
            var userOwnsProduct = await _productService.UsersOwnProduct(productId, HttpContext.GetUserIdFromContext());
            if (!userOwnsProduct)
                return BadRequest("You can edit only your own products");

            var updatedProd = await _productService.UpdateProductAsync(productId, request);
            return updatedProd != null ? Ok(updatedProd) : (IActionResult)NotFound(); 
        }

        [HttpPut(ApiRoutes.Products.BuyProduct)]
        public async Task<IActionResult> BuyProduct([FromRoute] Guid productId)
        {
            var buyerId = HttpContext.GetUserIdFromContext();
            var product = await _productService.GetProductAsync(productId);
            if (product == null)
            {
                return NotFound();
            }

            if (product.SellerId == buyerId)
            {
                return BadRequest("You can't buy your own product buddy");
            }

            if(product.ProductStatus == ProductStatus.Sold)
            {
                return BadRequest("You can't buy already bought product");
            }

            var boughtProduct = await _productService.BuyProduct(productId, buyerId);

            return boughtProduct != null ? Ok(new object[] { "Successfully bought", boughtProduct }) : (IActionResult)BadRequest("Purchase failed");

        }

    }
}

﻿using System;
using System.Linq;
using AnotherProductsShop.Contracts;
using Microsoft.AspNetCore.Mvc;
using AnotherProductsShop.Services;
using System.Threading.Tasks;
using AnotherProductsShop.Models;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using AnotherProductsShop.Extensions;
using System.Collections.Generic;

namespace AnotherProductsShop.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IJwtTokenService _jwtTokenService;

        public UserController(IUserService userService, IJwtTokenService jwtTokenService)
        {
            _userService = userService;
            _jwtTokenService = jwtTokenService;
        }

        [AllowAnonymous]
        [HttpGet("Registration")]
        public IActionResult RegistrationPage()
        {
            return View("Authentication");
        }
        
        
        
        [AllowAnonymous]
        [HttpGet(ApiRoutes.Users.GetAllUsers)]
        public async Task<IActionResult> GetAllUsers()
        {
            var users = await _userService.GetAllUsersAsync();
            List<DomainUser> domainUsers = new List<DomainUser>();

            foreach(User u in users)
            {
                domainUsers.Add(ConvertUserToDomainUser(u));
            }

            return Ok(domainUsers);

        }

        [HttpGet(ApiRoutes.Users.GetUser)]
        public async Task<IActionResult> GetUser([FromRoute] Guid userId)
        {
            var user = ConvertUserToDomainUser(await _userService.GetUserAsync(userId));
            return user != null ? Ok(user) : (IActionResult)NotFound();
        }


        [HttpPut(ApiRoutes.Users.UpdateUser)]
        public async Task<IActionResult> UpdateUser([FromRoute] Guid userId, [FromBody] UpdateUserRequest request)
        {
            if (!IsEmailValid(request.Email))
                return BadRequest("Enter valid E-Mail");
            if (!IsPasswordValid(request.Password))
                return BadRequest("Choose another password");

            var isUserMe = userId == HttpContext.GetUserIdFromContext();

            if (!isUserMe)
                return BadRequest("You can update only your own account");
            var isEmailFree = await _userService.IsEmailAvailable(request.Email);

            if (!isEmailFree)
                return BadRequest("Email is already taken");

            var updatedUser = ConvertUserToDomainUser(await _userService.UpdateUserAsync(userId, request));

            return updatedUser != null ? Ok(updatedUser) : (IActionResult)NotFound();
        }

        [AllowAnonymous]
        [HttpPost(ApiRoutes.Users.Register)]
        public async Task<IActionResult> CreateUser([FromForm]RegisterUserRequest request)
        {
            if (!IsEmailValid(request.Email))
                return BadRequest("Enter valid E-Mail");
            if (!IsPasswordValid(request.Password))
                return BadRequest("Choose another password");

            var isEmailFree = await _userService.IsEmailAvailable(request.Email);

            if (!isEmailFree)
                return BadRequest("Email is already taken");

            var user = new User(request.Email, request.Password);

            var isCreated = await _userService.CreateUserAsync(user);
            if (!isCreated)
                return BadRequest("User was not created due unhandled error!");

            string token = _jwtTokenService.CreateToken(user);

            string baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}";
            string locationUri = $"{baseUrl}/{ApiRoutes.Users.GetUser.Replace("{userId}", user.Id.ToString())}";
            return Created(locationUri, new RegisterUserResponse
            {
                User = ConvertUserToDomainUser(user),
                Token = token
            });

        }


        [HttpDelete(ApiRoutes.Users.DeleteUser)]
        public async Task<IActionResult> DeleteUser([FromRoute] Guid userId)
        {
            var isUserMe = userId == HttpContext.GetUserIdFromContext();

            if (!isUserMe)
                return BadRequest("You can delete only your own account");

            var deleted = await _userService.DeleteUserAsync(userId);
            return deleted ? Ok($"user with id {userId} was deleted!") : (IActionResult)NotFound();
        }

        [AllowAnonymous]
        [HttpPost(ApiRoutes.Users.Login)]
        public async Task<IActionResult> Login([FromBody] LoginUserRequest loginRequest)
        {
            if (!IsEmailValid(loginRequest.Email))
                return BadRequest("Enter valid E-Mail");
            if (!IsPasswordValid(loginRequest.Password))
                return BadRequest("Enter valid Password");

            var existingUser = await _userService.FindUserAsync(loginRequest.Email, loginRequest.Password);
            if (existingUser == null)
                return NotFound("User with these credentials ain't exists ;(");
            var token = _jwtTokenService.CreateToken(existingUser);

            return Ok($"token: {token}");



        }

        #region Helpers 
        private bool IsEmailValid(string emailAddress)
        {
            if (String.IsNullOrEmpty(emailAddress))
                return false;
            try
            {
                MailAddress m = new MailAddress(emailAddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private bool IsPasswordValid(string pass) =>
            Regex.Match(pass, @"(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,15})$").Success;

        private static DomainUser ConvertUserToDomainUser(User user)
        {
            var domainUser = new DomainUser
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email,
                Age = user.Age,
                RegistrationDate = user.RegistrationDate,
                UserRelatedProducts = user.UserRelatedProducts,
                Status = user.Status
            };

            return domainUser;
            #endregion
        }
    }
}

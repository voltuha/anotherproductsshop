﻿using System;
using System.Collections.Generic;
using AnotherProductsShop.Contracts;
using AnotherProductsShop.Models;
using System.Threading.Tasks;
using AnotherProductsShop.Data;
using Microsoft.EntityFrameworkCore;

namespace AnotherProductsShop.Services
{
    public class ProductService : IProductService
    {
        private readonly WebApiContext _webApiContext;

        public ProductService(WebApiContext webApiContext)
        {
            _webApiContext = webApiContext;
        }

        public async Task<bool> CreateProductAsync(Product product)
        {
            _webApiContext.Add(product);
            var created = await _webApiContext.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> DeleteProductAsync(Guid productId)
        {
            var product = await GetProductAsync(productId);
            if (product == null)
            {
                return false;
            }
            _webApiContext.Products.Remove(product);
            var deleted = await _webApiContext.SaveChangesAsync();
            return deleted > 0;
        }


        public async Task<List<Product>> GetAllProductsAsync() =>
            await _webApiContext.Products.ToListAsync(); 
        

        public async Task<Product> GetProductAsync(Guid productId) =>
            await _webApiContext.Products.SingleOrDefaultAsync(x => x.Id == productId);


        public async Task<Product> UpdateProductAsync(Guid productId, UpdateProductRequest request)
        {
            var product = await GetProductAsync(productId);
            if (product == null)
            {
                return null;
            }
            product.Title = request.Title;
            product.Price = request.Price;
            _webApiContext.Update(product);
            var updated = await _webApiContext.SaveChangesAsync();
            return updated > 0 ? product : null;
        }

        public async Task<bool> UsersOwnProduct(Guid productId, Guid userId)
        {
            var product = await GetProductAsync(productId);

            if (product.SellerId != userId)
                return false;
            return true;
        }

        public async Task<Product> BuyProduct(Guid productId, Guid buyerId)
        {
            var product = await GetProductAsync(productId);
            product.BuyerId = buyerId;
            product.ProductStatus = ProductStatus.Sold;
            _webApiContext.Update(product);
            var updated = await _webApiContext.SaveChangesAsync();
            return updated > 0 ? product : null;
        }
    }
}

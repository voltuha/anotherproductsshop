﻿using System;
using System.Linq;
using AnotherProductsShop.Models;
using AnotherProductsShop.Contracts;
using System.Collections.Generic;
using AnotherProductsShop.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace AnotherProductsShop.Services
{
    public class UserService : IUserService
    {
        private readonly WebApiContext _webApiContext;

        public UserService(WebApiContext webApiContext)
        {
            _webApiContext = webApiContext;
        }

        public async Task<bool> CreateUserAsync(User user)
        {
            await _webApiContext.Users.AddAsync(user);
            var created = await _webApiContext.SaveChangesAsync();
            return created > 0;

        }

        public async Task<List<User>> GetAllUsersAsync() =>
            await _webApiContext.Users.ToListAsync();


        public async Task<User> GetUserAsync(Guid userId) =>
            await _webApiContext.Users.SingleOrDefaultAsync(x => x.Id == userId);

        public async Task<User> UpdateUserAsync(Guid userId, UpdateUserRequest request)
        {
            var user = await GetUserAsync(userId);
            if (user == null)
            {
                return null;
            }

            user.Name = request.Name;
            user.Age = request.Age;
            user.Email = request.Email;

            _webApiContext.Users.Update(user);
            var updated = await _webApiContext.SaveChangesAsync();
            return updated > 0? user : null; 
        }

        public async Task<bool> DeleteUserAsync(Guid userId)
        {
            var user = await GetUserAsync(userId);
            if (user == null)
            {
                return false;
            }
            _webApiContext.Users.Remove(user);
            var deleted = await _webApiContext.SaveChangesAsync();
            return deleted > 0;
        }

        public async Task<User> FindUserByEmailAsync(string email)
        {
            var user = await _webApiContext.Users.SingleOrDefaultAsync(x => x.Email == email);
            return user;
        }

        public async Task<User> FindUserAsync(string email, string password)
        {
            var user = await _webApiContext.Users.SingleOrDefaultAsync(x => x.Email == email && x.Password == password);
            return user;
        }

        public async Task<bool> IsEmailAvailable(string email)
        {
            var existingUser = await _webApiContext.Users.SingleOrDefaultAsync(x => x.Email == email);
            return existingUser == null ? true : false;
        }

      
    }
}

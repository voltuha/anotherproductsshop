using System;
using System.Threading.Tasks;
using AnotherProductsShop.Contracts;
using AnotherProductsShop.Contracts.Facebook;
using AnotherProductsShop.Models;

namespace AnotherProductsShop.Services
{
    public class AccountService : IAccountService
    {
        private readonly IFacebookService _facebookService;
        private readonly IJwtTokenService _jwtTokenService;
        private readonly IUserService _userService;

        public AccountService(IJwtTokenService jwtTokenService, IFacebookService facebookService, IUserService userService)
        {
            _jwtTokenService = jwtTokenService;
            _facebookService = facebookService;
            _userService = userService;
        }

        public async Task<string> FacebookLoginAsync(FacebookLoginResource facebookLoginResource)
        {
            if (String.IsNullOrEmpty(facebookLoginResource.FacebookToken))
            {
                throw new Exception("Token is null or empty");
            }

            var facebookUser = await _facebookService.GetUserFromFacebookAsync(facebookLoginResource.FacebookToken);
            var domainUser = await _userService.FindUserByEmailAsync(facebookUser.Email);

            if (domainUser == null)
            {
                var newUser = new User
                {
                    Email = facebookUser.Email, Id = Guid.NewGuid(), Name = facebookUser.Name, Password = null,
                    Age = 18, Status = UserStatus.Active, RegistrationDate = DateTime.Now, UserRelatedProducts = null
                };
                await _userService.CreateUserAsync(newUser);
                var token = _jwtTokenService.CreateToken(newUser);
                return token;
            }

            return _jwtTokenService.CreateToken(domainUser);
        } 
    }
}
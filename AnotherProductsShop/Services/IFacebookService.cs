using System.Threading.Tasks;
using AnotherProductsShop.Contracts.Facebook;

namespace AnotherProductsShop.Services
{
    public interface IFacebookService
    {
        public Task<FacebookUserResource> GetUserFromFacebookAsync(string facebookToken);
    }
}
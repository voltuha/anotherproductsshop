﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AnotherProductsShop.Contracts;
using AnotherProductsShop.Models;

namespace AnotherProductsShop.Services
{
    public interface IProductService
    {
        public Task<Product> GetProductAsync(Guid productId);
        public Task<Product> UpdateProductAsync(Guid productId, UpdateProductRequest request);
        public Task<bool> CreateProductAsync(Product request);
        public Task<bool> DeleteProductAsync(Guid productId);
        public Task<List<Product>> GetAllProductsAsync();
        public Task<bool> UsersOwnProduct(Guid productId, Guid userId);
        public Task<Product> BuyProduct(Guid productId, Guid buyerId);

    }
}

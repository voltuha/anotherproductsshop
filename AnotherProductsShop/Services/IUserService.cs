﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AnotherProductsShop.Contracts;
using AnotherProductsShop.Models;

namespace AnotherProductsShop.Services
{
    public interface IUserService
    {
        public Task<bool> CreateUserAsync(User user);
        public Task<List<User>> GetAllUsersAsync();
        public Task<User> GetUserAsync(Guid userId);
        public Task<User> UpdateUserAsync(Guid userId, UpdateUserRequest request);
        public Task<bool> DeleteUserAsync(Guid userId);
        public Task<User> FindUserAsync(string email, string password);
        public Task<bool> IsEmailAvailable(string email);
        public Task<User> FindUserByEmailAsync(string email);


    }
}

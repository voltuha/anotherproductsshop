using System.Threading.Tasks;
using AnotherProductsShop.Contracts.Facebook;

namespace AnotherProductsShop.Services
{
    public interface IAccountService
    {
        public Task<string> FacebookLoginAsync(FacebookLoginResource facebookLoginResource);

    }
}
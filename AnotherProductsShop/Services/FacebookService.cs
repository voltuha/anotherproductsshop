using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AnotherProductsShop.Contracts.Facebook;
using Newtonsoft.Json;

namespace AnotherProductsShop.Services
{
    public class FacebookService : IFacebookService
    {
        private readonly HttpClient _httpClient;

        public FacebookService()
        {
            _httpClient = new HttpClient()
            {
                BaseAddress = new Uri("https://graph.facebook.com/v6.0/")
            };
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        public async Task<FacebookUserResource> GetUserFromFacebookAsync(string facebookToken)
        {
            var result = await GetAsync<dynamic>(facebookToken, "me", "fields=first_name,email");
            if (result == null)
            {
                throw new Exception("User from this token not exist");
            }
            
            var account = new FacebookUserResource()
            {
                Email = result.email,
                Name = result.first_name
            };

            return account;
        } 
        
        private async Task<T> GetAsync<T>(string accessToken, string endpoint, string args = null)
        {
            var response = await _httpClient.GetAsync($"{endpoint}?access_token={accessToken}&{args}");
            if (!response.IsSuccessStatusCode)
                return default(T);

            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(result);
        }
    }
}
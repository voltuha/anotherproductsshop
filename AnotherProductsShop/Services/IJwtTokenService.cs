﻿using System;
using AnotherProductsShop.Models;

namespace AnotherProductsShop.Services
{
    public interface IJwtTokenService
    {
        public string CreateToken(User user);
    }
}
